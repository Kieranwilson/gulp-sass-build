# Gulp build SASS with browsersync

> Gulp build task I created for SASS

I originally created a gulpfile to compile js and sass down to minified version with sourcemaps intact for debugging.

The SASS sourcemappings were not working however and after a bit of investigation, it is the ruby sass modules that are
required for SASS sourcemaps. So I ammended the gulpfile to have two variants for those that have ruby and those that do not.

I then created the default gulp command to list all available commands and made the gulp tasks informative.