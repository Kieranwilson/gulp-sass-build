var gulp = require('gulp');
var noRubySass = require('gulp-sass');
var rubySass = require('gulp-ruby-sass');
var browserSync = require('browser-sync').create();
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var clone = require('gulp-clone');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var merge = require('gulp-merge');
var autoprefixer = require('gulp-autoprefixer');
var gutil = require('gulp-util');

var browserSyncOptions = {
    proxy: "redkite.com",
    notify: false,
    ghostMode: {
        clicks: false,
        forms: false,
        scroll: false
    }
};

var browserSyncWatchFiles = [
    './css/*.min.css',
    './**/*.php',
    './js/*.js'
];

gulp.task('styles-ruby', function() {
    gutil.log(gutil.colors.white.bgRed('THIS COMMAND REQUIRES RUBY AND SASS, IF YOU ARE GETTING ERRORS MAKE SURE THEY ARE INSTALLED'));

    var source = rubySass('./sass/theme.scss', {
            sourcemap: true,
            stopOnError: true
        })
        .pipe(plumber({
            errorHandler: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9'],
            cascade: false
        }))

    var pipe1 = source.pipe(clone())
        .pipe(sourcemaps.write('.', {
            includeContent: false,
            sourceRoot: '../sass'
        }))
        .pipe(gulp.dest('./css'))


    var pipe2 = source.pipe(clone())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('.', {
            includeContent: false,
            sourceRoot: '../sass'
        }))
        .pipe(gulp.dest('./css'));

    return;

    // TODO investigate issue with merge, does not always return correctly from this
    // This was done wrong and should use event-stream instead, see https://www.npmjs.com/package/gulp-clone for example
    //return merge(pipe1, pipe2);

});

gulp.task('styles-no-ruby', function() {
    var stream = gulp.src('./sass/theme.scss')
        .pipe(plumber({
            errorHandler: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(noRubySass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9'],
            cascade: false
        }))

    var pipe1 = source.pipe(clone())
        .pipe(gulp.dest('./css'))


    var pipe2 = source.pipe(clone())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./css'));

    return;

    // TODO investigate issue with merge, does not always return correctly from this
    //return merge(pipe1, pipe2);

});


gulp.task('minify-no-map', function() {
  return gulp.src('./css/theme.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(plumber({
            errorHandler: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('./css/'));
});

// Run:
// gulp browser-sync
// Starts browser-sync task for starting the server.
gulp.task('browser-sync', function() {
    browserSync.init(browserSyncWatchFiles, browserSyncOptions);
});

// Run:
// gulp watch-ruby
// Starts watcher. Watcher runs gulp sass task on changes - REQUIRES RUBY AND SASS
gulp.task('watch-ruby', function () {
    gutil.log(gutil.colors.white.bgRed('THIS COMMAND REQUIRES RUBY AND SASS, IF YOU ARE GETTING ERRORS MAKE SURE THEY ARE INSTALLED'));
    gulp.watch('./sass/**/**/*.scss', ['styles-ruby']);
});

// Run:
// gulp watch
// Starts watcher. Watcher runs gulp sass task on changes - NO RUBY REQUIREMENT, NO SOURCEMAPS
gulp.task('watch-no-ruby', function () {
    gulp.watch('./sass/**/**/*.scss', ['styles-no-ruby']);
});

// Run:
// gulp watch-bs
// Starts watcher with browser-sync. Browser-sync reloads page automatically on your browser
gulp.task('watch-bs-no-ruby', ['browser-sync', 'watch-no-ruby'], function () { });

// Run:
// gulp watch-bs
// Starts watcher with browser-sync. Browser-sync reloads page automatically on your browser
gulp.task('watch-bs-ruby', ['browser-sync', 'watch-ruby'], function () { });

gulp.task('default', function () {
    gutil.log('The full list of available commands are:');
    gutil.log('gulp styles-ruby       | builds the sass file into the .css and .min.css with source maps REQUIRES RUBY AND SASS INSTALLED VIA GEM');
    gutil.log('gulp styles-no-ruby    | builds the sass file into the .css and .min.css without source maps');
    gutil.log('gulp minify-no-map     | builds the .min.css from the .css and ignores sourcemaps');
    gutil.log('gulp browser-sync      | will watch for css/javascript/php changes and automatically replace/refresh content on the browser WILL REQUIRE CHANGING browserSyncOptions AT THE TOP OF gulpfile.js TO PROXY YOUR SERVER CONFIG');
    gutil.log('gulp watch-ruby        | will watch for scss changes and run gulp-styles-ruby when detected REQUIRES RUBY AND SASS INSTALLED VIA GEM');
    gutil.log('gulp watch-no-ruby     | will watch for scss changes and run gulp-styles-no-ruby when detected');
    gutil.log('gulp watch-bs-ruby     | combination of watch and browser sync, the command will automatically detect scss changes and rebuild the files triggering the refresh, also will update after js and php files REQUIRES RUBY AND SASS INSTALLED VIA GEM');
    gutil.log('gulp watch-bs-no-ruby  | combination of watch and browser sync, the command will automatically detect scss changes and rebuild the files triggering the refresh, also will update after js and php files');
});